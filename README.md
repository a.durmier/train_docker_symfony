# Processus lors de l'installation
* [tutoriel](https://www.youtube.com/watch?v=tRI6KFNKfFo)
* mkdir docker_symfony
* cd docker_symfony
* touch docker-compose.yml
```bash
version: "3.8"
services:

  db:
    image: mysql
    container_name: db_docker_symfony
    restart: always
    volumes:
      - db-data:/var/lib/mysql
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
    networks:
      - dev

  phpmyadmin:
    image: phpmyadmin
    container_name: phpmyadmin_docker_symfony
    restart: always
    depends_on:
      - db
    ports:
      - "8080:80"
    environment:
      PMA_HOST: db
    networks:
      - dev

  maildev:
      image: maildev/maildev
      container_name: maildev_docker_symfony
      command: bin/maildev --web 80 --smtp 25 --hide-extensions STARTTLS
      ports:
        - "8081:80"
      restart: always
      networks:
        - dev

  www:
      build: php
      container_name: www_docker_symfony
      ports:
        - "8741:80"
      volumes:
        - ./php/vhosts:/etc/apache2/sites-enabled
        - ./:/var/www
      restart: always
      networks:
        - dev


networks:
  dev:

volumes:
  db-data:
```
* mkdir php (création du dossier php à la racine)
* touch Dockerfile
```bash
FROM php:7.4-apache

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf \
\
    &&  apt-get update \
    &&  apt-get install -y --no-install-recommends \
        locales apt-utils git libicu-dev g++ libpng-dev libxml2-dev libzip-dev libonig-dev libxslt-dev unzip \
\
    &&  echo "en_US.UTF-8 UTF-8" > /etc/locale.gen  \
    &&  echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
    &&  locale-gen \
\
    &&  curl -sS https://getcomposer.org/installer | php -- \
    &&  mv composer.phar /usr/local/bin/composer \
\
    &&  curl -sS https://get.symfony.com/cli/installer | bash \
    &&  mv /root/.symfony/bin/symfony /usr/local/bin \
\
    &&  docker-php-ext-configure \
            intl \
    &&  docker-php-ext-install \
            pdo pdo_mysql opcache intl zip calendar dom mbstring gd xsl \
\
    &&  pecl install apcu && docker-php-ext-enable apcu

WORKDIR /var/www/
```
* mkdir vhosts (création dans le dossier php)
* touch vhosts.conf
```bash
<VirtualHost *:80>
    ServerName localhost

    DocumentRoot /var/www/project/public
    DirectoryIndex /index.php

    <Directory /var/www/project/public>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        FallbackResource /index.php
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeeScript assets
    # <Directory /var/www/project>
    #     Options FollowSymlinks
    # </Directory>

    # optionally disable the fallback resource for the asset directories
    # which will allow Apache to return a 404 error when files are
    # not found instead of passing the request to Symfony
    <Directory /var/www/project/public/bundles>
        FallbackResource disabled
    </Directory>
    ErrorLog /var/log/apache2/project_error.log
    CustomLog /var/log/apache2/project_access.log combined

    # optionally set the value of the environment variables used in the application
    #SetEnv APP_ENV prod
    #SetEnv APP_SECRET <app-secret-id>
    #SetEnv DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name"
</VirtualHost>
```
* Création du projet Symfony dans le container:
```bash
docker exec www_docker_symfony composer create-project symfony/website-skeleton project  
```